FROM tiangolo/uvicorn-gunicorn-fastapi:python3.8
COPY requirements.txt requirements.txt
RUN apt-get update
RUN apt-get install tesseract-ocr -y
RUN pip install -r requirements.txt
COPY ./app /app