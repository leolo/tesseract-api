from fastapi import FastAPI, File, UploadFile, HTTPException
from fastapi.middleware.cors import CORSMiddleware
from ocr import handle_ocr
import logging

app = FastAPI(title='TesseractAPI')

origins = [
    "*",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

@app.post('/')
def optical_text_recognition(picture: UploadFile = File(None)):
    if not picture:
        raise HTTPException(status_code=400, detail='File part is missing')
    return handle_ocr(picture)