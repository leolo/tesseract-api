import os
import re
import shutil
from PIL import Image
import cv2
import pytesseract
from fastapi import UploadFile
from sonic import SearchClient
from pymongo import MongoClient
import sys
from fastapi.logger import logger
# ... other imports
import logging

UPLOAD_FOLDER = 'uploaded_files'

def handle_ocr(picture: UploadFile) -> str:
    setup_directory()

    new_filename = get_filename(picture.filename)
    with open(new_filename, 'wb') as new_file:
        shutil.copyfileobj(picture.file, new_file)

    # load the example image and convert it to grayscale
    image = cv2.imread(new_filename)
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    
    # apply thresholding to preprocess the image
    gray = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]

    # apply median blurring to remove any blurring
    gray = cv2.medianBlur(gray, 3)

    # save the processed image in the /static/uploads directory
    ofilename = get_filename(f"{os.getpid()}.png")
    cv2.imwrite(ofilename, gray)

    img = Image.open(ofilename)
    ocr_result = pytesseract.image_to_string(img)

    teardown_directory(new_filename, ofilename)

    return get_category(ocr_result)

def setup_directory():
    if not os.path.exists(UPLOAD_FOLDER):
        os.makedirs(UPLOAD_FOLDER)

def get_filename(filename: str) -> str:
    return os.path.join(UPLOAD_FOLDER, filename)

def teardown_directory(*args):
    for filename in args:
        os.remove(filename)

def get_category(ocr_result: str):
    gunicorn_logger = logging.getLogger('gunicorn.error')
    logger.handlers = gunicorn_logger.handlers
    if __name__ != "main":
        logger.setLevel(gunicorn_logger.level)
    else:
        logger.setLevel(logging.DEBUG)

    bill = []
    bill_lines = ocr_result.split("\n")
    collections = get_database()

    with SearchClient("sonic", 1491, "SecretPassword") as querycl:
        for bill_line in bill_lines:
            logger.warning(bill_line)
            match = re.search("^[a-zA-Z]*", bill_line)

            if match.group():
                result = querycl.query("bills", "products", match.group())

                if result:
                    item_result = collections.find_one({"_id": result[0]})
                    bill.append(f"{bill_line} {item_result['_source']['categories']}")

    return bill

def get_database():
    CONNECTION_STRING = "mongodb://root:oink@mongodb"
    client = MongoClient(CONNECTION_STRING)
    db = client["bills"]
    return db["items"]
