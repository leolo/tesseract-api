import base64
import unittest
from fastapi.testclient import TestClient
from main import app

class TestMain(unittest.TestCase):
    def test_main(self):
        client = TestClient(app)
        test_picture_path = '/app/tests/test_picture.jpg'
        with open(test_picture_path, 'rb') as picture:
            files = {'picture': picture}
            response = client.post('/', files=files)
        self.assertEqual(response.status_code, 200)

    def test_main_without_file(self):
        client = TestClient(app)
        response = client.post('/')
        self.assertEqual(response.status_code, 400)