# Tesseract API

### A minimal tesseract api powered by the python api framework FastAPI.
### Send files and the api returns the recogniced text.

### Prerequisits:
- Git
- Docker

### Get it with Git:

```Git clone 'url'```


### Build the image with docker:

```docker build --name <image_name> .```


### Run the container with docker:

```docker run -t <container-tag> -p 80:80 <image_name>```


## Use the api with docker-compose:

```docker-compose build```

And then:

```docker-compose up```

## Documentation
An openapi documentation is available at: [URI]/docs

## Run the unittests:
Attach your shell to the container:

```docker exec -it <container id> bash```

Run the unittests:

```python3 -m unittest```